FROM python:2

MAINTAINER Coordinación de Ecoinformática <ecoinformatica@conabio.gob.mx>

RUN apt-get update && \
    apt-get install -y unzip \
                       curl \
                       poppler-utils \
                       tesseract-ocr \
                       tesseract-ocr-eng \
                       tesseract-ocr-spa \
                       gdal-bin \
                       npm

RUN echo 'deb http://httpredir.debian.org/debian jessie-backports main' > /etc/apt/sources.list.d/jessie-backports.list

# Default to UTF-8 file.encoding
ENV LANG C.UTF-8

ENV JAVA_VERSION 8u72
ENV JAVA_DEBIAN_VERSION 8u72-b15-1~bpo8+1

# see https://bugs.debian.org/775775
# and https://github.com/docker-library/java/issues/19#issuecomment-70546872
ENV CA_CERTIFICATES_JAVA_VERSION 20140324

RUN set -x \
    && apt-get update \
    && apt-get install -y \
        openjdk-8-jre-headless="$JAVA_DEBIAN_VERSION" \
        ca-certificates-java="$CA_CERTIFICATES_JAVA_VERSION" \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*

# see CA_CERTIFICATES_JAVA_VERSION notes above
RUN /var/lib/dpkg/info/ca-certificates-java.postinst configure

WORKDIR /app

# TIKA
ENV TIKA_VERSION 1.12
ENV TIKA_APP_URL https://www.apache.org/dist/tika/tika-app-$TIKA_VERSION.jar
ENV TIKA_DIR tika-$TIKA_VERSION

RUN curl -sSL https://people.apache.org/keys/group/tika.asc -o /tmp/tika.asc \
    && gpg --import /tmp/tika.asc \
    && curl -sSL "$TIKA_APP_URL.asc" -o /tmp/tika-app-${TIKA_VERSION}.jar.asc \
    && NEAREST_TIKA_APP_URL=$(curl -sSL http://www.apache.org/dyn/closer.cgi/${TIKA_APP_URL#https://www.apache.org/dist/}\?asjson\=1 \
        | awk '/"path_info": / { pi=$2; }; /"preferred":/ { pref=$2; }; END { print pref " " pi; };' \
        | sed -r -e 's/^"//; s/",$//; s/" "//') \
    && echo "Nearest mirror: $NEAREST_TIKA_APP_URL" \
    && mkdir $TIKA_DIR \
    && curl -sSL "$NEAREST_TIKA_APP_URL" -o $TIKA_DIR/tika-app-${TIKA_VERSION}.jar \
    && gpg --verify /tmp/tika-app-${TIKA_VERSION}.jar.asc $TIKA_DIR/tika-app-${TIKA_VERSION}.jar \
    && apt-get clean -y && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# OPENNLP
ENV OPENNLP_VERSION 1.5.3
ENV OPENNLP_APP_URL https://www.apache.org/dist/opennlp/opennlp-$OPENNLP_VERSION/apache-opennlp-$OPENNLP_VERSION-bin.tar.gz
ENV OPENNLP_DIR apache-opennlp-$OPENNLP_VERSION

RUN curl -sSL https://www.apache.org/dist/opennlp/KEYS -o /tmp/opennlp.asc \
    && gpg --import /tmp/opennlp.asc \
    && curl -sSL "$OPENNLP_APP_URL.asc" -o /tmp/apache-opennlp-${OPENNLP_VERSION}-bin.tar.gz.asc \
    && NEAREST_OPENNLP_APP_URL=$(curl -sSL http://www.apache.org/dyn/closer.cgi/${OPENNLP_APP_URL#https://www.apache.org/dist/}\?asjson\=1 \
        | awk '/"path_info": / { pi=$2; }; /"preferred":/ { pref=$2; }; END { print pref " " pi; };' \
        | sed -r -e 's/^"//; s/",$//; s/" "//') \
    && echo "Nearest mirror: $NEAREST_OPENNLP_APP_URL" \
    && curl -sSL "$NEAREST_OPENNLP_APP_URL" -o apache-opennlp-$OPENNLP_VERSION-bin.tar.gz \
    && gpg --verify /tmp/apache-opennlp-${OPENNLP_VERSION}-bin.tar.gz.asc apache-opennlp-$OPENNLP_VERSION-bin.tar.gz \
    && tar zxf apache-opennlp-$OPENNLP_VERSION-bin.tar.gz && rm apache-opennlp-$OPENNLP_VERSION-bin.tar.gz \
    && rm -rf /tmp/* /var/tmp/*

# PDFBox
ENV PDFBOX_VERSION 1.8.11
ENV PDFBOX_APP_URL https://www.apache.org/dist/pdfbox/$PDFBOX_VERSION/pdfbox-app-$PDFBOX_VERSION.jar
ENV PDFBOX_DIR pdfbox-$PDFBOX_VERSION

RUN curl -sSL https://www.apache.org/dist/pdfbox/KEYS -o /tmp/pdfbox.asc \
    && gpg --import /tmp/pdfbox.asc \
    && curl -sSL "$PDFBOX_APP_URL.asc" -o /tmp/pdfbox-app-$PDFBOX_VERSION.jar.asc \
    && NEAREST_PDFBOX_APP_URL=$(curl -sSL http://www.apache.org/dyn/closer.cgi/${PDFBOX_APP_URL#https://www.apache.org/dist/}\?asjson\=1 \
        | awk '/"path_info": / { pi=$2; }; /"preferred":/ { pref=$2; }; END { print pref " " pi; };' \
        | sed -r -e 's/^"//; s/",$//; s/" "//') \
    && echo "Nearest mirror: $NEAREST_PDFBOX_APP_URL" \
    && mkdir $PDFBOX_DIR \
    && curl -sSL "$NEAREST_PDFBOX_APP_URL" -o $PDFBOX_DIR/pdfbox-app-$PDFBOX_VERSION.jar \
    && gpg --verify /tmp/pdfbox-app-$PDFBOX_VERSION.jar.asc $PDFBOX_DIR/pdfbox-app-$PDFBOX_VERSION.jar \
    && rm -rf /tmp/* /var/tmp/*

# Enviroment variables
ENV TIKA_BIN /app/$TIKA_DIR/tika-app-$TIKA_VERSION.jar
ENV PDFBOX_BIN /app/$PDFBOX_DIR/pdfbox-app-$PDFBOX_VERSION.jar
ENV OPENNLP_BIN /app/$OPENNLP_DIR/bin/opennlp
ENV PDF_TO_TEXT_BIN /usr/bin/pdftotext
ENV PDF_INFO_BIN /usr/bin/pdfinfo
