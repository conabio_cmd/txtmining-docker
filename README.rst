Docker Image for Txtmining Library
==================================

This image provides the basic requirements for installing and running
the `txtmining library`_.

Images installs:
    * OpenJDK JRE
    * Apache TIKA
    * Apache OPENNLP
    * Apache PDFBox
    * PDFinfo
    * PDFtotext

It also the following enviroment variables are defined:
    * TIKA_BIN
    * PDFBOX_BIN
    * OPENNLP_BIN
    * PDF_TO_TEXT_BIN
    * PDF_INFO_BIN

.. _txtmining library: https://bitbucket.org/conabio_cmd/text-mining
